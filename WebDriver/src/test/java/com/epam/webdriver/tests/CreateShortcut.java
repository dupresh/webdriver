package com.epam.webdriver.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateShortcut extends BaseTest {
    public static final String NAME_FOR_PREPARETION = "��� ������";
    private static final String NAME_OF_SHORTCUT = "test";

    @BeforeClass
    public void logIn()
    {
        step.logInUser(user1, password1);
        step.creteNewShortcut(NAME_FOR_PREPARETION);
        step.clickCreateShortcut();
    }

    @Test
    public void createShortcut()
    {
        step.openShortcut();
        step.creteNewShortcut(NAME_OF_SHORTCUT);
        step.chooseMyShortcut();
        step.clickCreateShortcut();
        Assert.assertTrue(step.isTestShortcutCreated(NAME_OF_SHORTCUT));
    }

    @AfterClass
    public void deleteShortcutAndLogout()
    {
        step.openShortcut();
        step.deleteShortcut();
        step.logOutUser();
    }
}
