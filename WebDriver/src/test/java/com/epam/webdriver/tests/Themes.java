package com.epam.webdriver.tests;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Themes extends BaseTest {

	private static final String FILEPATH = "";

	@BeforeClass
	public void logIn()
	{
		step.logInUser(user1, password1);
	}
	
	@Test
	public void Theme() throws AWTException
	{
		step.GoToThemes();
		step.ChooseFileWhichIsNotImage(FILEPATH);
		Assert.assertTrue(step.isErrorMessageApeared());
	}
	
	@AfterTest
	public void logOut()
	{
		step.logOutUser();
	}
}
