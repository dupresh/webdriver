package com.epam.webdriver.tests;

import java.awt.AWTException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendMailWithEmotion extends BaseTest {

	private static final String SUBJECT = "EmTest";

	@BeforeClass
	public void logIn()
	{
		step.logInUser(user1, password1);
	}

	@Test
	public void sendMailWithEmotion() throws AWTException
	{
		step.clickWrite();
		step.EditReciver(user1);
		step.EditSubject(SUBJECT);
		List<String> emotionList = step.emotionAttach();
		step.ClickSend();
		Assert.assertTrue(step.isLetterInMailBox(SUBJECT));
		step.clickOnMessegeWithSubject(SUBJECT);
		Assert.assertEquals(emotionList, step.emotionInLetter());
	}

	@AfterClass
	public void cleanInboxAndLogOut()
	{
		step.RemoveAllLetterFromInbox();
		step.logOutUser();
	}
}
