package com.epam.webdriver.tests;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MainMailBox extends BaseTest {

	private static final String SUBJECT = "test";
	private static final String PATH_OF_FILE_MORE_25MB = "";

	@BeforeClass
	public void logIn()
	{
		step.logInUser(user1, password1);
	}
	
	@Test
	public void MainMailBox() throws AWTException
	{
		step.AttachFileAndSendLetter(user2, SUBJECT, PATH_OF_FILE_MORE_25MB);
		Assert.assertTrue(step.isWarningMessageAppeared());
	}
	
	@AfterClass
	public void logOut()
	{
		step.logOutUser();
	}
}
