package com.epam.webdriver.tests;


import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class CheckLetterInSpam extends BaseTest {
	
	private final String SUBJECT1 = "test1";
	final String SUBJECT2 = "test2";
	
	@Test
	public void checkLetterInSpam()
	{		
		step.logInUser(user1, password1);
		String textOfLetter = "test letter's text";
		step.sendLetter(user2, SUBJECT1, textOfLetter);
		step.logOutUser();
		step.logInUser(user2, password2);
		step.sendLetterToSpam(SUBJECT1);
		step.logOutUser();
		step.logInUser(user1, password1);
		step.sendLetter(user2, SUBJECT2, textOfLetter);
		step.logOutUser();
		step.logInUser(user2, password2);
		step.OpenSpamPage();
		Assert.assertTrue(step.isLetterInSpam(SUBJECT2));
	}
	
	@AfterClass
	public void remoteAllLetters()
	{
		step.RemoveAllLettersFromSpam();
		step.logOutUser();
		step.RemoveAllLetterFromSent();
		step.logOutUser();
		step.logInUser(user1, password1);
		step.RemoveAllLetterFromInbox();
		step.logOutUser();
		step.closeBrowser();
	}
}
