package com.epam.webdriver.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ChangeTheme extends BaseTest {
    @BeforeClass
    public void logIn()
    {
        step.logInUser(user1, password1);
    }

    @Test
    public void changeTheme()
    {
        step.clickSettingButtom();
        step.clickThemeButtom();
        step.setBeachTheme();
        Assert.assertTrue(step.isThemeWasSet());
    }

    @AfterClass
    public void logOut()
    {
        step.clickSettingButtom();
        step.clickThemeButtom();
        step.setLightTheme();
        step.logOutUser();
    }
}
