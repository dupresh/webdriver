package com.epam.webdriver.tests;

import java.util.ResourceBundle;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.epam.webdriver.step.Steps;

public class BaseTest {

	protected static Steps step = new Steps();;
	protected ResourceBundle resource = ResourceBundle.getBundle("user");
	protected String user1 = resource.getString("user1");
	protected String user2 = resource.getString("user2");
	protected String user3 = resource.getString("user3");
	protected String password1 = resource.getString("password1");
	protected String password2 = resource.getString("password2");
	protected String password3 = resource.getString("password3");
	
	@BeforeClass
	public void start() {
		step.signBrowser();
	}
	
	@AfterClass
	public void end()
	{
		step.closeBrowser();
	}
	
}
