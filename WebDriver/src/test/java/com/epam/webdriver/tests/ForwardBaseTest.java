package com.epam.webdriver.tests;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class ForwardBaseTest extends BaseTest {

	private static final String SUBJECT = "test attach";
	private static final String PATH = "";
	private static final String TEXTOFLETTER = "_";
	private static final String SUBJECT2 = "simple test";

	@Test
	public void forWardTest() throws AWTException
	{
		step.logInUser(user2, password2);
		step.OpenSettingPageFromMail();
		step.GoToForwardPage();
		step.AddForwardAdress(user3);
		step.logOutUser();
		step.logInUser(user3, password3);
		step.AcceptForward();
		step.logOutUser();
		step.logInUser(user2, password2);
		step.OpenSettingPageFromMail();
		step.GoToForwardPage();
		step.CreateFilter(user1);
		step.logOutUser();
		step.logInUser(user1, password1);
		step.AttachFileAndSendLetter(user2, SUBJECT, PATH);
		step.sendLetter(user2, SUBJECT2, TEXTOFLETTER);
		step.logOutUser();
		step.logInUser(user2, password2);
		Assert.assertTrue(step.isLetterWithAttachinTrash(SUBJECT));
		Assert.assertTrue(step.isTheLetterIsNotImportant(SUBJECT2));
		step.logOutUser();
		step.logInUser(user3, password3);
		Assert.assertTrue(step.isLetterInMailBox(SUBJECT2));
	}
	
	@AfterClass
	public void returnAllSettings()
	{
		step.RemoveAllLetterFromInbox();
		step.logOutUser();
		step.logInUser(user1, password1);
		step.RemoveAllLetterFromSent();
		step.logOutUser();
		step.logInUser(user2, password2);
		step.RemoveAllLetterFromInbox();
		step.GoToForwardPage();
		step.TurnOffForward();
		step.logOutUser();
	}
}
