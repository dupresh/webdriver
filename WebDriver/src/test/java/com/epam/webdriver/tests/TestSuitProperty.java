package com.epam.webdriver.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class TestSuitProperty {

    private final static Logger logger = Logger.getLogger(TestSuitProperty.class);

    @BeforeSuite
    public void testBeforeSuite() {
        logger.info("Before suit");
    }

    @AfterSuite
    public void testAfterSuite() {
        logger.info("After suit");
    }

    @BeforeTest
    public void testBeforeTest() {
        logger.info("Before test");
    }

    @AfterTest
    public void testAfterTest() {
        logger.info("After suit");
    }
}
