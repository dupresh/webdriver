package com.epam.webdriver.utils;

import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/**
 * Created by Анастасия on 20.08.2015.
 */
public class RobotUtil {
    private static final int TIME_OF_DELAY = 3000;
    private final static Logger logger = Logger.getLogger(RobotUtil.class);

    public static void javaRobotForAttachFile(StringSelection ss)
    {
        try {
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.delay(TIME_OF_DELAY);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
        }catch(AWTException e)
        {
            logger.error("Robot is broken");
        }
    }
}
