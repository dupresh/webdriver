package com.epam.webdriver.step;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.epam.webdriver.page.*;
import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Steps {

	private static WebDriver driver;

	public void signBrowser()
	{
		driver = new FirefoxDriver();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		LoggerUtil.info("in browser");
	}
	
	public void closeBrowser()
	{
		driver.quit();
	}
	
	public void logInUser(String user, String pass)
	{
		LogInPage login = new LogInPage(driver);
		login.openPage();
		login.login(user, pass);
		LoggerUtil.info("User logged in");
	}
	
	public void logOutUser()
	{
		MailPage mail = new MailPage(driver);
		mail.logOut();
		LoggerUtil.info("User logged out");
	}
	
	public void sendLetter(String reciver, String subject, String textOfLetter)
	{
		MailPage mail = new MailPage(driver);
		mail.openPage();
		mail.clickWrite();
		mail.EditREciever(reciver);
		mail.EditSubject(subject);
		mail.EditTextOfLetter(textOfLetter);
		mail.ClickSend();
		LoggerUtil.info("Letter was sent");
	}
	
	public void sendLetterToSpam(String subject)
	{
		MailPage mail = new MailPage(driver);
		mail.SendLetterToSpam(subject);
		LoggerUtil.info("Letter in spam");
	}
	
	public void OpenSpamPage()
	{
		SpamPage spam = new SpamPage(driver);
		spam.openPage();
	}
	
	public boolean isLetterInSpam(String subject)
	{
		SpamPage spam = new SpamPage(driver);
		spam.openPage();
		return spam.isTheLetterInSpam(subject);
	}
	
	public void RemoveAllLettersFromSpam()
	{
		SpamPage spam = new SpamPage(driver);
		spam.openPage();
		spam.RemoveAllLetterFromSpam();
		LoggerUtil.info("All letters were removed from spam");
	}
	
	public void RemoveAllLetterFromInbox()
	{
		MailPage mail = new MailPage(driver);
		mail.openPage();
		mail.DeleteAllLetters();
		LoggerUtil.info("All letters were deleted");
	}
	
	public void RemoveAllLetterFromSent()
	{
		SentPage sent = new SentPage(driver);
		sent.openPage();
		sent.deleteAllLetters();
		LoggerUtil.info("All letters were deleted");
	}
	
	public void OpenSettingPageFromMail()
	{
		MailPage mail = new MailPage(driver);
		mail.openPage();
		mail.OpenSettingFromMail();
		LoggerUtil.info("Setting was opened");
	}
	
	public void GoToForwardPage()
	{
		SettingPage set = new SettingPage(driver);
		set.GoToForward();
		LoggerUtil.info("Forward was opened");
	}
	
	public void AddForwardAdress(String user)
	{
		SettingPage set = new SettingPage(driver);
		set.AddForwardAdress(user);
		LoggerUtil.info("Add user for forward");
	}
	
	public void AcceptForward()
	{
		MailPage mail = new MailPage(driver);
		mail.AcceptForward();
		LoggerUtil.info("Farward was accepted");
	}
	
	public void RadioButtomForwardCopy()
	{
		SettingPage set = new SettingPage(driver);
		set.ClickRadioButtomForwardCopy();
	}
	
	public void CreateFilter(String user)
	{
		SettingPage set = new SettingPage(driver);
		set.CreateFilter(user);
		LoggerUtil.info("Filter was created");
	}
	
	public void AttachFileAndSendLetter(String reciver, String subject, String path) throws AWTException
	{
		MailPage mail = new MailPage(driver);
		mail.clickWrite();
		mail.EditREciever(reciver);
		mail.EditSubject(subject);
		mail.AttachFile(path);
		mail.ClickSend();
		LoggerUtil.info("File was attached and the letter was sent");
	}
	
	public boolean isLetterWithAttachinTrash(String subject)
	{
		TrashPage trash = new TrashPage(driver);
		trash.openPage();
		return trash.isLetterFromUser1WithAttachInTrash(subject);
	}
	
	public boolean isLetterInMailBox(String subject)
	{
		MailPage mail = new MailPage(driver);
		mail.openPage();
		return mail.isTheLetterInMailBox(subject);
	}
	
	public void TurnOffForward()
	{
		SettingPage set = new SettingPage(driver);
		set.openPage();
		set.TurnOffForward();
		LoggerUtil.info("Forwards was turnt off");
	}
	
	public void clickWrite()
	{
		MailPage mail = new MailPage(driver);
		mail.clickWrite();
	}
	
	public boolean isTheLetterIsNotImportant(String subject)
	{
		MailPage mail = new MailPage(driver);
		return mail.isTheLetterIsNotImportant(subject);
	}
	
	public boolean isWarningMessageAppeared()
	{
		MailPage mail = new MailPage(driver);
		return mail.isWarningMessageAppeared();
	}
	
	public void GoToThemes()
	{
		MailPage mail = new MailPage(driver);
		mail.GoToThemes();
	}
	
	public void ChooseFileWhichIsNotImage(String filepath) throws AWTException
	{
		ThemePage theme = new ThemePage(driver);
		theme.ChooseNotImageFile(filepath);
		LoggerUtil.info("Find not image file");
	}
	
	public boolean isErrorMessageApeared()
	{
		ThemePage theme = new ThemePage(driver);
		return theme.isErrorMessageApeared();
	}
	
	public void EditReciver(String reciver)
	{
		MailPage mail = new MailPage(driver);
		mail.EditREciever(reciver);
	}
	
	public void EditSubject(String subject)
	{
		MailPage mail = new MailPage(driver);
		mail.EditSubject(subject);
	}
	
	public void ClickSend()
	{
		MailPage mail = new MailPage(driver);
		mail.ClickSend();
		LoggerUtil.info("Send email");
	}

	public List<String> emotionAttach() throws AWTException
	{
		MailPage mail = new MailPage(driver);
		return mail.emotionAttach();
	}

	public void clickOnMessegeWithSubject(String subject)
	{
		MailPage mail = new MailPage(driver);
		mail.clickOnMailWithSubject(subject);
		LoggerUtil.info("Letter was opened");
	}

	public List<String> emotionInLetter()
	{
		MailPage mail = new MailPage(driver);
		return mail.getEmotionsGoomoji();
	}

	public void clickSettingButtom()
	{
		ThemePage theme = new ThemePage(driver);
		theme.clickSetBut();
	}

	public void clickThemeButtom()
	{
		ThemePage theme = new ThemePage(driver);
		theme.clickThemeButtom();
	}

	public void setBeachTheme()
	{
		ThemePage theme = new ThemePage(driver);
		theme.setBeachTheme();
		LoggerUtil.info("Theme was sets");
	}

	public void openShortcut()
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		shortcut.openPage();
	}

	public void creteNewShortcut(String name)
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		shortcut.creteNewShortcut(name);
		LoggerUtil.info("Shortcut was created");
	}

	public void clickCreateShortcut()
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		shortcut.clickCreateNewShortcutBut();
	}

	public void chooseMyShortcut()
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		shortcut.chooseMyShortcut();
	}

	public boolean isTestShortcutCreated(String name)
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		return shortcut.isShortcutCreated(name);
	}

	public void deleteShortcut()
	{
		ShortcutPage shortcut = new ShortcutPage(driver);
		shortcut.deleteShortcut();
		LoggerUtil.info("Shortcut was created");
	}

	public boolean isThemeWasSet()
	{
		ThemePage theme = new ThemePage(driver);
		return theme.isThemeWasSet();
	}

	public void setLightTheme()
	{
		ThemePage theme = new ThemePage(driver);
		theme.chooseLightTheme();
		LoggerUtil.info("Return first theme");
	}

}
