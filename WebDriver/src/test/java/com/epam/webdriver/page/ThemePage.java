package com.epam.webdriver.page;

import static com.epam.webdriver.utils.RobotUtil.javaRobotForAttachFile;


import java.awt.AWTException;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.epam.webdriver.utils.WaitUtils;

public class ThemePage extends AbstractPage {

	private static final String BASE_URL = "https://mail.google.com/mail/#settings/themes";
	private static final String UPLOAD_FILE_FRAME = "//div[@class='Xf-dn-xn Xf-dn-Bn']";
	private static final String MESSAGE_WITH_ERROR = "//div[@class='d-Jb d-Jb-Lb d-Jb-Ob']";
	private final String ERROR_MESSAGE_WHCH_APPEAR = "There was an upload error.  Dismiss";

	@FindBy(xpath = "//div[@class='J-J5-Ji T-I T-I-ax7 a94 T-I-JO T-I-Zf-aw2']")
	private WebElement MyPhotoClick;
	
	@FindBy(xpath = "//div[@class='Xf-Zf-Xb-wh']")
	private WebElement buttonUploadPhotos;
	
	@FindBy(xpath = "//div[@class='a-b-c d-u d-u-Q d-u-W d-u-G-H']")
	private WebElement buttonSelectPhotosFromYourComputer;
	
	@FindBy(xpath = "//div[@class='d-Jb d-Jb-Lb d-Jb-Ob']/span")
	private WebElement messageUploadError;

	@FindBy(xpath = "//div[@class='aos T-I-J3 J-J5-Ji']")
	private WebElement settingButtom;

	@FindBy(xpath = "//div[text()='����']")
	private WebElement themeLine;

	@FindBy(xpath = "//span[@data-tooltip='���� (��������: iStockPhoto)']")
	private WebElement beach;

	@FindBy(xpath="//div[@class='a70 aXjCH']")
	private WebElement themeFrame;

	@FindBy(xpath = "//span[@class='Kj-JD-K7-Jq a8Z']")
	private WebElement saveAndClose;

	@FindBy(xpath = "//div[@class='vh'][contains(text(),'saved')]")
	private WebElement messageThatChoosenThemeWasSet;

	@FindBy(xpath = "//span[@class='sf']")
	private WebElement curentTheme;

	@FindBy(xpath = "//span[text()='Light']")
	private WebElement lightTheme;

	public ThemePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void ChooseNotImageFile(String filepath) throws AWTException
	{
		MyPhotoClick.click();
		buttonUploadPhotos.click();
		WaitUtils.isElementPresent(driver, UPLOAD_FILE_FRAME);
		buttonSelectPhotosFromYourComputer.click();
		File file = new File(filepath);
		StringSelection stringSel = new StringSelection(file.getAbsolutePath());
		javaRobotForAttachFile(stringSel);
	}
	
	public boolean isErrorMessageApeared() {
		return WaitUtils.isElementPresent(driver, MESSAGE_WITH_ERROR, TIME_OF_WAIT) && messageUploadError.getText().equals(
						ERROR_MESSAGE_WHCH_APPEAR);
	}

	public void clickSetBut()
	{
		settingButtom.click();
	}

	public void clickThemeButtom()
	{
		themeLine.click();
	}

	public void setBeachTheme()
	{
		WaitUtils.waitForElementVisible(driver, themeFrame, TIME_OF_WAIT);
		beach.click();
		saveAndClose.click();
	}

	public boolean isThemeWasSet()
	{
		return curentTheme.getText().equals("Beach");
	}

	public void chooseLightTheme() {
		lightTheme.click();
		saveAndClose.click();
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("ThemePage opened");
	}

}
