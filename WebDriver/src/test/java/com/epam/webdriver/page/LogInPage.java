package com.epam.webdriver.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.epam.webdriver.utils.LoggerUtil;

public class LogInPage extends AbstractPage {

	private final static String BASE_URL = "http://www.gmail.com";

	@FindBy(id = "Email")
	private WebElement inputMail;
	
	@FindBy(id = "next")
	private WebElement buttomNext;
	
	@FindBy(id = "Passwd")
	private WebElement inputPass;
	
	@FindBy(id = "signIn")
	private WebElement buttonSignIn;


	public LogInPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username, String password) {
		inputMail.sendKeys(username);
		buttomNext.click();
		inputPass.sendKeys(password);
		buttonSignIn.click();
		LoggerUtil.info("User in gmail");
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("Login Page opened");
	}
	
}
