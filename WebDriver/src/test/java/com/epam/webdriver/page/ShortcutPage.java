package com.epam.webdriver.page;

import com.epam.webdriver.tests.CreateShortcut;
import com.epam.webdriver.utils.WaitUtils;
import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShortcutPage extends AbstractPage {

    private static final String BASE_URL = "https://mail.google.com/mail/#settings/labels";
    private static final String DELETE_FRAME = "//div[@class='Kj-JD']";

    @FindBy(xpath = "//button[@class='alZ']")
    private WebElement createShortcut;

    @FindBy(xpath = "//div[@class='Kj-JD']")
    private WebElement newShortcutWindow;

    @FindBy(xpath = "//input[@class='xx']")
    private  WebElement nameOfShortcut;

    @FindBy(name = "ok")
    private WebElement okButtom;

    @FindBy(xpath = "//input[@class='ajJ']")
    private WebElement checkBoxOfUnderShortcut;

    @FindBy(xpath = "//select[@class='xx']")
    private  WebElement arrowWithShortcut;

    @FindBy(xpath = "//option[@value="+ CreateShortcut.NAME_FOR_PREPARETION+"]")
    private WebElement myShortcut;

    @FindBy(xpath = "//div[@class='TH aih J-J5-Ji aij']")
    private WebElement arrowNearMyShortcut;

    @FindBy(xpath = "//div[@class='aio aip']")
    private WebElement testShortcut;

    @FindBy(xpath = "//span[@act='lpe']")
    private WebElement deleteShortcut;

    @FindBy(xpath = "//div[@class='Kj-JD']")
    private WebElement deleteFrame;

    public void creteNewShortcut(String name)
    {
        createShortcut.click();
        WaitUtils.waitForElementVisible(driver, newShortcutWindow, TIME_OF_WAIT);
        nameOfShortcut.sendKeys(name);
    }

    public void clickCreateNewShortcutBut()
    {
        okButtom.click();
    }

    public void chooseMyShortcut()
    {
        if(!checkBoxOfUnderShortcut.isSelected())
        {
            checkBoxOfUnderShortcut.click();
        }
        arrowWithShortcut.click();
        myShortcut.click();
    }

    public boolean isShortcutCreated(String name)
    {
        arrowNearMyShortcut.click();
        return testShortcut.getText().equals(name);
    }

    public void deleteShortcut()
    {
        deleteShortcut.click();
        WaitUtils.isElementPresent(driver, DELETE_FRAME, TIME_OF_WAIT);
        okButtom.click();
    }

    @Override
    public void openPage() {
        driver.navigate().to(BASE_URL);
       LoggerUtil.info("Shortcut was opened");
    }

    public ShortcutPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
