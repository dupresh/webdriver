package com.epam.webdriver.page;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {

	protected WebDriver driver;
	protected final int TIME_OF_WAIT = 100;
	public abstract void openPage();

	public AbstractPage(WebDriver driver)
	{
		this.driver = driver;
	}
}
