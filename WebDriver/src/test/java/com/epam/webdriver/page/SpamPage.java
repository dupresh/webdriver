package com.epam.webdriver.page;

import com.epam.webdriver.utils.WaitUtils;
import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpamPage extends AbstractPage {

	private final static String BASE_URL = "https://mail.google.com/mail/#spam";
	


	private final static String TOPIC_OF_THE_LETTER = "//div[@class='y6']/span/b[contains(text(),'%s')]";

	@FindBy(xpath = "//div[@class='T-Jo-auh']")
	private WebElement SelectAllSpamLetters;
	
	@FindBy(xpath = "//div[@class='T-I J-J5-Ji aFk T-I-ax7 ar7 T-I-JO T-I-Zf-aw2']")
	private WebElement NotSpam;
	
	public boolean isTheLetterInSpam(String subject) {
		return WaitUtils.isElementNotPresent(driver, String.format(TOPIC_OF_THE_LETTER, subject));
	}
	
	public void RemoveAllLetterFromSpam()
	{
		SelectAllSpamLetters.click();
		NotSpam.click();
	}
	
	public SpamPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("SpamPage opened");
	}

	
}
