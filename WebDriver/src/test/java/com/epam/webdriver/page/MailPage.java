package com.epam.webdriver.page;

import java.awt.AWTException;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.epam.webdriver.utils.WaitUtils;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.epam.webdriver.utils.LoggerUtil;

import static com.epam.webdriver.utils.RobotUtil.javaRobotForAttachFile;

public class MailPage extends AbstractPage {

	private final static String BASE_URL = "https://mail.google.com/mail";
	private final String LOADING_BAR_WHEN_ATTACHED_FILE = "//div[@class='dR']";
	private final String TOPIC_OF_THE_LETTER = "//div[@class='y6']/span/b[contains(text(),'%s')]";
	private final String WARNING_FILE_MORE_25_MB = "//div[@class='Kj-JD']";
	private final String IMAGE_ATTACHMENT = "//img[@alt='Attachments']";
	private final String IMPORTANT = "Important";
	private final String THEME_SETTING_FRAME = "//div[@class='Kj-JD a8j']";
	private final String ATTRIBUTE_GOOMOJI = "goomoji";

	@FindBy(css = "div.T-I.J-J5-Ji.T-I-KE.L3.T-I-JO")
	private WebElement buttomWrite;
	
	@FindBy(css = "textarea.vO")
	private WebElement editReceiver;

	@FindBy(css = "input.aoT")
	private WebElement editSubject;

	@FindBy(xpath = "//div[@class='Am Al editable LW-avf']")
	private WebElement editTextOfLetter;
	
	@FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO T-I-atl L3 T-I-Zf-aw2']")
	private WebElement sendButtom;
	
	@FindBy(xpath = ".//*[@id=':3k']")
	private WebElement LetterSubject;
	
	@FindBy(css = "div.oZ-jc.T-Jo.J-J5-Ji")
	private WebElement chooseLetter;
	
	@FindBy(css = "div.asl.T-I-J3.J-J5-Ji")
	private WebElement buttonAddToSpam;
	
	@FindBy(xpath = ".//*[@id='gb']")
	private WebElement ArrowNearAcc;
	
	@FindBy(xpath = ".//*[@id='gb_71']")
	private WebElement userExit;
	
	@FindBy(xpath = "//div[@class='T-Jo-auh']")
	private WebElement SelectAllLetters;
	
	@FindBy(xpath = "//div[@class='ar9 T-I-J3 J-J5-Ji']")
	private WebElement DeleteAllLetters;
	
	@FindBy(xpath = "//div[@class='aos T-I-J3 J-J5-Ji']")
	private WebElement SettingButton;
	
	@FindBy(xpath = "//div[@class='J-N-Jz']")
	private WebElement SettingParam;
	
	@FindBy(xpath = "//td[@id=':3q']")
	private WebElement FirstMailInInbox;
	
	@FindBy(xpath = "//a[@target='_blank']")
	private WebElement AcceptForwardLink;
	
	@FindBy(xpath = "//div[@class='a1 aaA aMZ'")
	private WebElement AttachmentClick;
	
	@FindBy(xpath = "//img[@class='ajz']")
	private WebElement imgInfoAboutLetter;

	@FindBy(xpath = "//div[@class='ajB gt']")
	private WebElement infoAboutLetter;
	
	@FindBy(xpath = "//div[@class='J-N-Jz']")
	private WebElement ThemeClick;
	
	@FindBy(xpath = "//div[@class='QT aaA aMZ']")
	private WebElement EmotionIcon;

	@FindBy(xpath = "//button{@class='a8v a8t a8t Je a8w']")
	private WebElement smiles;

	@FindBy(xpath = "//div[@class='e263a a8r']")
	private WebElement smileNumOne;

	@FindBy(xpath = "//div@[class='e1f60a a8r']")
	private WebElement smileNumTwo;

	@FindBy(xpath = "//div[@class='y6']/span/b[contains(text(),'%s')]")
	private WebElement letterSubject;

	@FindBy(xpath = ".//img[@class='CToWUd']")
	private List<WebElement> emotionsInLetter;

	public MailPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void clickWrite()
	{
		buttomWrite.click();
	}
	
	public void EditREciever(String reciver)
	{
		editReceiver.sendKeys(reciver);
	}
	
	public void EditSubject(String subject)
	{
		editSubject.sendKeys(subject);
	}
	
	public void EditTextOfLetter(String textOfLetter)
	{
		editTextOfLetter.sendKeys(textOfLetter);
	}
	
	public void ClickSend()
	{
		sendButtom.click();
	}
	
	public void SendLetterToSpam(String subject)
	{
		chooseLetter.click();
		WaitUtils.waitForElementVisible(driver, buttonAddToSpam, TIME_OF_WAIT);
		buttonAddToSpam.click();
		WaitUtils.waitForElementInvisibility(driver,TOPIC_OF_THE_LETTER, TIME_OF_WAIT);
	}
	
	public void logOut()
	{
		ArrowNearAcc.click();
		userExit.click();		
	}
	
	public void DeleteAllLetters()
	{
		SelectAllLetters.click();
		DeleteAllLetters.click();
	}
	
	public void GoToThemes()
	{
		SettingButton.click();
		ThemeClick.click();
		WaitUtils.isElementPresent(driver, THEME_SETTING_FRAME, TIME_OF_WAIT);
	}
	
	public void OpenSettingFromMail()
	{
		SettingButton.click();
		SettingParam.click();
	}
	
	public void AcceptForward()
	{
		FirstMailInInbox.click();
		AcceptForwardLink.click();
	}
	
	public void AttachFile(String path)
	{
		AttachmentClick.click();
		File file = new File(path);
		StringSelection stringSel = new StringSelection(file.getAbsolutePath());
		javaRobotForAttachFile(stringSel);
		WaitUtils.waitForElementInvisibility(driver,
				LOADING_BAR_WHEN_ATTACHED_FILE, TIME_OF_WAIT);
	}
	
	public boolean isTheLetterInMailBox(String subject) {
		return WaitUtils.isElementNotPresent(driver, String.format(TOPIC_OF_THE_LETTER, subject));
	}
	
	public boolean isTheLetterIsNotImportant(String subject)
	{
		WebElement letter = driver.findElement(By.xpath(String.format(TOPIC_OF_THE_LETTER, subject)));
		letter.click();
		if (WaitUtils.isElementNotPresent(driver, IMAGE_ATTACHMENT))
		{
			imgInfoAboutLetter.click();
		}
		return (!infoAboutLetter.getText().contains(IMPORTANT));
	}
	
	public boolean isWarningMessageAppeared() {
		return WaitUtils.isElementPresent(driver, WARNING_FILE_MORE_25_MB);
	}

	public List<String> emotionAttach() throws AWTException
	{
		EmotionIcon.click();
		List<String> emoticonsNames = new ArrayList<String>();
		smiles.click();
		smileNumOne.click();
		emoticonsNames.add(smileNumOne.getAttribute(ATTRIBUTE_GOOMOJI));
		smileNumTwo.click();
		emoticonsNames.add(smileNumTwo.getAttribute(ATTRIBUTE_GOOMOJI));
		return emoticonsNames;
	}

	public void clickOnMailWithSubject(String subject) {
		driver.navigate().refresh();
		chooseLetter.click();
	}

	public List<String> getEmotionsGoomoji()
	{
		List<String> emotionInLeter = new ArrayList<String>();
		for(WebElement emotion : emotionsInLetter)
		{
			emotionInLeter.add(emotion.getAttribute(ATTRIBUTE_GOOMOJI));
		}
		return emotionInLeter;
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("SendPage opened");

	}

}
