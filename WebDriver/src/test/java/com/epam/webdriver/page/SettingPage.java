package com.epam.webdriver.page;

import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SettingPage extends AbstractPage{

	private final static String BASE_URL = "https://mail.google.com/mail/#settings/general";

	@FindBy(xpath = "//a[@href='https://mail.google.com/mail/#settings/fwdandpop']")
	private WebElement ForwardElement;
	
	@FindBy(xpath = "//input[@type='button']")
	private WebElement AddForwardMailButtom;
	
	@FindBy(xpath = "//input[@type='text']")
	private WebElement InputForwardAdress;
	
	@FindBy(name = "next")
	private WebElement buttomNext;
	
	@FindBy(xpath = "//input[@type = 'radio']")
	private WebElement radioButtomForwardCopy;
	
	@FindBy(xpath = "//a[@href='https://mail.google.com/mail/#settings/filters']")
	private WebElement FilterElement;
	
	@FindBy(xpath = "//span[@class = 'sA']")
	private WebElement CreateNewFilter;
	
	@FindBy(xpath = "//input[@class = 'ZH nr aQa']")
	private WebElement FilterSetFrom;
	
	@FindBy(xpath = "//label[@for = ':3b']")
	private WebElement CheckBoxAttachment;
	
	@FindBy(xpath = "//div[@class ='acM']")
	private WebElement NextFiltersSetLink;
	
	@FindBy(xpath = "//label[@class = 'aD'][@for =':7j']")
	private WebElement CheckBoxDelete;
	
	@FindBy(xpath = "//label[@class = 'aD'][@for =':7l']")
	private WebElement CheckBoxImportant;
	
	@FindBy(xpath = "//div[@class ='T-I J-J5-Ji Zx acL T-I-atl L3 T-I-JO T-I-Zf-aw2']")
	private WebElement CompleteFilterCreation;
	
	@FindBy(xpath = "//input[@type='submit'][@value='Proceed']")
	private WebElement buttonProceedForConfirmAdress;

	@FindBy(xpath = "//button[@class='J-at1-auR'][@name='ok']")
	private WebElement buttonConfirmOk;
	
	@FindBy(xpath = "//label[@for=':bu']")
	private WebElement RadioTurnOffForward;
	
	@FindBy(xpath = "//button[@guidedhelpid='save_changes_button']")
	private WebElement SaveChangesButtom;
	
	public SettingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void GoToForward()
	{
		ForwardElement.click();
	}
	
	public void AddForwardAdress(String user)
	{
		AddForwardMailButtom.click();
		InputForwardAdress.sendKeys(user);
		buttomNext.click();
		driver.switchTo().activeElement();
		buttonProceedForConfirmAdress.click();
		driver.switchTo().parentFrame();
		buttonConfirmOk.click();
	}
	
	public void ClickRadioButtomForwardCopy()
	{
		radioButtomForwardCopy.click();
	}
	
	public void CreateFilter(String user)
	{
		FilterElement.click();
		CreateNewFilter.click();
		FilterSetFrom.sendKeys(user);
		CheckBoxAttachment.click();
		NextFiltersSetLink.click();
		CheckBoxDelete.click();
		CheckBoxImportant.click();
		CompleteFilterCreation.click();
	}
	
	public void TurnOffForward()
	{
		ForwardElement.click();
		RadioTurnOffForward.click();
		SaveChangesButtom.click();
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("Setting page opened");
	}
}
