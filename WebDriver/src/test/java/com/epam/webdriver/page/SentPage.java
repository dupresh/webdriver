package com.epam.webdriver.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.epam.webdriver.utils.LoggerUtil;

public class SentPage extends AbstractPage {

	private final static String BASE_URL = "https://mail.google.com/mail/#sent";

	@FindBy(xpath = "//div[@class='T-Jo-auh']")
	private WebElement SelectAllLetters;
	
	@FindBy(xpath = "//div[@class='ar9 T-I-J3 J-J5-Ji']")
	private WebElement DeleteAllLetters;
	
	public void deleteAllLetters()
	{
		SelectAllLetters.click();
		DeleteAllLetters.click();
	}
	
	public SentPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("SentPage opened");
	}
}
