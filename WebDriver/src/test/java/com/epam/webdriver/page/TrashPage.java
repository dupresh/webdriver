package com.epam.webdriver.page;

import com.epam.webdriver.utils.LoggerUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.epam.webdriver.utils.WaitUtils;

public class TrashPage extends AbstractPage {

	private final static String TOPIC_OF_THE_LETTER = "//div[@class='y6']/span/b[contains(text(),'%s')]";
	private final static String IMAGE_ATTACHMENT = "//img[@alt='Attachments']";
	private final String IMPORTANT = "Important";
	private static final String BASE_URL = "https://mail.google.com/mail/#trash";

	@FindBy(xpath = "//div[@class='yW']/span[@class='zF'][ancestor::div[@class='ae4 UI UJ']]")
	private WebElement newLetter;
	
	@FindBy(xpath = "//img[@class='ajz']")
	private WebElement imgInfoAboutLetter;

	@FindBy(xpath = "//div[@class='ajB gt']")
	private WebElement infoAboutLetter;

	public TrashPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public boolean isLetterFromUser1WithAttachInTrash(String subject)
	{

		int i = 0;
		if (WaitUtils.isElementNotPresent(driver, String.format(TOPIC_OF_THE_LETTER, subject))== true) {
			WebElement letter = driver.findElement(By.xpath(String.format(TOPIC_OF_THE_LETTER, subject)));
			letter.click();
			if (WaitUtils.isElementPresent(driver, IMAGE_ATTACHMENT)) {
				imgInfoAboutLetter.click();
				return infoAboutLetter.getText().contains(IMPORTANT);
			}
			else
				return false;
		}
		else
			return false;
	}
	
	@Override
	public void openPage() {
		driver.navigate().to(BASE_URL);
		LoggerUtil.info("TrashPage opened");
	}
}
